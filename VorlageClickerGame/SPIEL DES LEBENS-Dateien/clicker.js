'use strict'; 

// --------------------------------------------------------------------------
// --------  Diese (Start-)Werte später ggf. aus Speicher/Cloud ziehen ------

	var Handlungen = new Object();
	Handlungen = {'Klicken': 0, 'Lesen': 0,'Schreiben': 0};

	var PunkteFaktor = new Object();
	PunkteFaktor = {'Lesen': 3,'Schreiben': 2};


	var BoostFaktor = new Object();
	BoostFaktor = {'Lesen': 1,'Schreiben': 1};

	var Energie = new Object();
	Energie = {'Kraft': 800,'Laune': 800};

	var Energieverlust = new Object();
	Energieverlust = {'Kraft': 1,'Laune': 1.5};
// -----------------------------------------------------------------




// Interfaceelemente erfassen: ----------
	var eingabe = new Object();
	eingabe['Schreiben'] = document.getElementById('Schreibbutton');
	eingabe['Lesen'] = document.getElementById('Lesebutton');
	eingabe['Ruhe'] = document.getElementById('Ruhebutton');
	eingabe['Laune'] = document.getElementById('Launebutton');
	var ausgabe = new Object();
	ausgabe['Schreiben'] = document.getElementById('Schreibausgabe');
	ausgabe['Lesen'] = document.getElementById('Leseausgabe');
	ausgabe['Bildungsindex'] = document.getElementById('Bildungsindex');
	ausgabe['Gesamtklickzahl'] = document.getElementById('Gesamtklickzahl');
	ausgabe['Kraft'] = document.getElementById('Kraft');
	ausgabe['Laune'] = document.getElementById('Laune');

// Buttonklicks erfassen: ---------------
	eingabe['Schreiben'].addEventListener("touchstart", function(e) {e.preventDefault(); basisHandlung('Schreiben', 'Spieler');}); 
	eingabe['Schreiben'].addEventListener("click", function(e) {e.preventDefault(); basisHandlung('Schreiben', 'Spieler');}); 
	eingabe['Lesen'].addEventListener("touchstart", function(e) {e.preventDefault(); basisHandlung('Lesen', 'Spieler');}); 
	eingabe['Lesen'].addEventListener("click", function(e) {e.preventDefault(); basisHandlung('Lesen', 'Spieler');});  

	eingabe['Ruhe'].addEventListener("touchstart", function(e) {e.preventDefault(); energieHandlung('Kraft');}); 
	eingabe['Ruhe'].addEventListener("click", function(e) {e.preventDefault(); energieHandlung('Kraft');});
	eingabe['Laune'].addEventListener("touchstart", function(e) {e.preventDefault(); energieHandlung('Laune');}); 
	eingabe['Laune'].addEventListener("click", function(e) {e.preventDefault(); energieHandlung('Laune');}); 
// -------------------------------	


EnergieAnzeige("Kraft");				// Bei Spielstart Energiewert anzeigen
EnergieAnzeige("Laune");				// Bei Spielstart Energiewert anzeigen


function basisHandlung(Aktion, Akteur) { 
		Handlungen.Klicken++;		// 	Klickzahl um eins erhöhen
		aktualisiereEnergie(Akteur);
		var gesamtEnergieFaktor = gesamtEnergieErmitteln();
		var SuperBoostFaktor = BoostFaktor[Aktion]*gesamtEnergieFaktor; 
		Handlungen[Aktion] = Handlungen[Aktion] + SuperBoostFaktor; 
		var bildungsindex = kaufmaennischRunden(Handlungen.Lesen*PunkteFaktor['Lesen'] + Handlungen.Schreiben*PunkteFaktor['Schreiben'], 2);
		ausgabe[Aktion].innerHTML = Math.round(Handlungen[Aktion]);
		ausgabe['Bildungsindex'].innerHTML = bildungsindex;
		ausgabe['Gesamtklickzahl'].innerHTML = Handlungen.Klicken;
    }

function energieHandlung(Aktion) {
	Energie[Aktion]++;
	EnergieAnzeige(Aktion);
}

function gesamtEnergieErmitteln() {	// multipliziert alle einzelnen Energiewerte zu einem "GesamtEnergieFaktor"
	var gesamtEnergieFaktor = 1;
	Object.keys(Energie).forEach(function(key) {		// durchläuft alle Energiefaktoren und rechnet sie nacheinander rein
	gesamtEnergieFaktor = gesamtEnergieFaktor * (Energie[key]/1000);
	});
	return gesamtEnergieFaktor;
	}

function aktualisiereEnergie(Akteur) {
	if (Akteur == "Spieler") {				// nur Spielerhandlungen machen Spieler müssen (Nicht Ghostwriter etc...) 
		Object.keys(Energie).forEach(function(key) {		// durchläuft alle Werte
			Energie[key] = Energie[key]-Energieverlust[key];  								// Energiewert verringern
			EnergieAnzeige(key);
			});
	} /* else if (Akteur == "Game") {			// bei Spielstart soll aktueller Energierwert eingeblendet werden
		Object.keys(Energie).forEach(function(key) {		// durchläuft alle Werte 
			EnergieAnzeige(key);
			}); 
	} */
}

function EnergieAnzeige(key) {
	ausgabe[key].value = Energie[key];
	let selector = '.Anzeige.' + key;
	document.querySelector(selector).innerHTML = Energie[key];
}

// ----------- diverse allegemeine Ergänzungsfunktionen: -----------

function kaufmaennischRunden(zahl, nachkommastellen) {	// rundet und kürzt Zahl auf Nachkommastellen
	return parseFloat(zahl).toFixed(nachkommastellen);
}