'use strict';

// --------------------------------------------------------------------------
// --------  Diese (Start-)Werte später ggf. aus Speicher/Cloud ziehen ------
var GhostEigenschaften = new Object();
GhostEigenschaften["Ghostwriter"] = {'Frequenz': 200, 'Dauer': 5};
GhostEigenschaften["Ghostreader"] = {'Frequenz': 100, 'Dauer': 4};

var BoosterEigenschaften = new Object();
BoosterEigenschaften["Schreibbooster"] = {'Dauer': 5, 'Faktor': 10};
BoosterEigenschaften["Lesebooster"] = {'Dauer': 7, 'Faktor': 5};
// ---------------------------------------------------------------------------


eingabe['Schreibbooster'] = document.getElementById('Schreibbooster');
eingabe['Lesebooster'] = document.getElementById('Lesebooster');
eingabe['Ghostwriter'] = document.getElementById('Ghostwriter');
eingabe['Ghostreader'] = document.getElementById('Ghostreader');

aktualisiereGhostArbeiterZeit("Ghostwriter");
aktualisiereGhostArbeiterZeit("Ghostreader");

aktualisiereBoostFaktor("Lesebooster");
aktualisiereBoostFaktor("Schreibbooster");


// Buttonklicks erfassen: --------
	eingabe['Schreibbooster'].addEventListener("touchstart", function(e) {e.preventDefault(); schreibBooster(BoosterEigenschaften.Schreibbooster.Faktor, BoosterEigenschaften.Schreibbooster.Dauer);}); 
	eingabe['Schreibbooster'].addEventListener("click", function(e) {e.preventDefault(); schreibBooster(BoosterEigenschaften.Schreibbooster.Faktor, BoosterEigenschaften.Schreibbooster.Dauer);});

	eingabe['Lesebooster'].addEventListener("touchstart", function(e) {e.preventDefault(); leseBooster(BoosterEigenschaften.Lesebooster.Faktor, BoosterEigenschaften.Lesebooster.Dauer);}); 
	eingabe['Lesebooster'].addEventListener("click", function(e) {e.preventDefault(); leseBooster(BoosterEigenschaften.Lesebooster.Faktor, BoosterEigenschaften.Lesebooster.Dauer);});

	eingabe['Ghostwriter'].addEventListener("touchstart", function(e) {e.preventDefault(); ghostWriter(GhostEigenschaften.Ghostwriter.Frequenz, GhostEigenschaften.Ghostwriter.Dauer);}); 
	eingabe['Ghostwriter'].addEventListener("click", function(e) {e.preventDefault(); ghostWriter(GhostEigenschaften.Ghostwriter.Frequenz, GhostEigenschaften.Ghostwriter.Dauer);});


	eingabe['Ghostreader'].addEventListener("touchstart", function(e) {e.preventDefault(); ghostReader(GhostEigenschaften.Ghostreader.Frequenz, GhostEigenschaften.Ghostreader.Dauer);}); 
	eingabe['Ghostreader'].addEventListener("click", function(e) {e.preventDefault(); ghostReader(GhostEigenschaften.Ghostreader.Frequenz, GhostEigenschaften.Ghostreader.Dauer);});

// -------------------------------	

function ghostWriter(ghostwriterwortfrequenz, ghostwritedauer) { 	// Wert 1: Anzahl der Worte/Sekunde  |  Wert 2: Dauer in Sekunden
		var target = 'gwcounter';
		eingabe['Ghostwriter'].disabled = true; 				// Button für die Dauer des Boosts inaktiv machen
		var element = document.getElementById(target);
		element.seconds = ghostwritedauer;
		calculateAndShowClockForGW(target);
		var effektel = document.getElementById("Schreibdiv");
		effektel.classList.add("ghostrobot");
		eingabe["Ghostwriter"].querySelector('.robotzeit').style.display = "none";
		ghostWriting(ghostwriterwortfrequenz, ghostwritedauer);
    }

function ghostReader(ghostreaderwortfrequenz, ghostreaddauer) { 	// Wert 1: Anzahl der Worte/Sekunde  |  Wert 2: Dauer in Sekunden
		var target = 'grcounter';
		eingabe['Ghostreader'].disabled = true; 				// Button für die Dauer des Boosts inaktiv machen
		var element = document.getElementById(target);
		element.seconds = ghostreaddauer;
		calculateAndShowClockForGR(target);
		var effektel = document.getElementById("Lesediv");
		effektel.classList.add("ghostrobot");
		eingabe["Ghostreader"].querySelector('.robotzeit').style.display = "none";
		ghostReading(ghostreaderwortfrequenz, ghostreaddauer);
    }

function schreibBooster(faktor, dauer) { 	
		var target = 'writingboostcounter';
		eingabe['Schreibbooster'].disabled = true; 				// Button für die Dauer des Boosts inaktiv machen
		var element = document.getElementById(target);
		element.seconds = dauer;
		calculateAndShowClock(target, 'Schreiben');
		eingabe['Schreiben'].classList.add("boost");
		BoostFaktor.Schreiben = faktor;
    }

function leseBooster(faktor, dauer) { 	
		var target = 'readingboostcounter';
		eingabe['Lesebooster'].disabled = true; 				// Button für die Dauer des Boosts inaktiv machen
		var element = document.getElementById(target);
		element.seconds = dauer;
		calculateAndShowClock(target, 'Lesen');
		eingabe['Lesen'].classList.add("boost");
		BoostFaktor.Lesen = faktor;
    }

function leadingzero(number) {
	return (number < 10) ? '0' + number : number;
}

function calculateAndShowClock(target, aktion) { 
	var element = document.getElementById(target);
	if (element.seconds >= 0) {
		element.timerId = window.setTimeout(calculateAndShowClock, 1000, target, aktion);
		var h = Math.floor(element.seconds / 3600);
		var m = Math.floor((element.seconds % 3600) / 60);
		var s = element.seconds % 60;
		element.innerHTML = leadingzero(h) + ':' + leadingzero(m) + ':' + leadingzero(s);
		element.seconds--;
	} else { // Zeit abgelaufen:
		var element = document.getElementById(target);
		element.innerHTML = "";
		element.closest("button").disabled = false;
		BoostFaktor[aktion] = 1; 
		eingabe[aktion].classList.remove("boost");
		return false;
	}
}

function calculateAndShowClockForGW(target) {
	var element = document.getElementById(target);
	if (element.seconds >= 0) {
		element.timerId = window.setTimeout(calculateAndShowClockForGW, 1000, target);
		var h = Math.floor(element.seconds / 3600);
		var m = Math.floor((element.seconds % 3600) / 60);
		var s = element.seconds % 60;
		element.innerHTML = leadingzero(h) + ':' + leadingzero(m) + ':' + leadingzero(s);
		element.seconds--;
	} else { // Zeit abgelaufen: 
		var element = document.getElementById(target);
		element.innerHTML = "";
		element.closest("button").disabled = false;
		var effektel = document.getElementById("Schreibdiv");
		effektel.classList.remove("ghostrobot");
		eingabe["Ghostwriter"].querySelector('.robotzeit').style.display = "inline";
		return false;
	}
}

function calculateAndShowClockForGR(target) {
	var element = document.getElementById(target);
	if (element.seconds >= 0) {
		element.timerId = window.setTimeout(calculateAndShowClockForGR, 1000, target);
		var h = Math.floor(element.seconds / 3600);
		var m = Math.floor((element.seconds % 3600) / 60);
		var s = element.seconds % 60;
		element.innerHTML = leadingzero(h) + ':' + leadingzero(m) + ':' + leadingzero(s);
		element.seconds--;
	} else { // Zeit abgelaufen: 
		var element = document.getElementById(target);
		element.innerHTML = "";
		element.closest("button").disabled = false;
		var effektel = document.getElementById("Lesediv");
		effektel.classList.remove("ghostrobot");
		eingabe["Ghostreader"].querySelector('.robotzeit').style.display = "inline";
		return false;
	}
}

function ghostWriting(ghostwriterwortfrequenz, ghostwritedauer) {
	var frequenz = 1000/ghostwriterwortfrequenz;
	var dauer = ghostwritedauer*1000;
	basisHandlung('Schreiben', 'Ghost'); 	// erster Schreibakt, weil setIntervall erst nach Ablauf des ersten Intervalls startet
	var gwIntervall = setInterval(function(){ basisHandlung('Schreiben', 'Ghost'); }, frequenz);	
	setTimeout(function(){ clearInterval(gwIntervall); }, dauer); 
}

function ghostReading(ghostreaderwortfrequenz, ghostreaddauer) {
	var frequenz = 1000/ghostreaderwortfrequenz;
	var dauer = ghostreaddauer*1000;
	basisHandlung('Lesen', 'Ghost'); 	// erster Schreibakt, weil setIntervall erst nach Ablauf des ersten Intervalls startet
	var grIntervall = setInterval(function(){ basisHandlung('Lesen', 'Ghost'); }, frequenz);	
	setTimeout(function(){ clearInterval(grIntervall); }, dauer); 
}
 
function aktualisiereGhostArbeiterZeit(target) {
	eingabe[target].querySelector('.robotzeit').innerHTML = GhostEigenschaften[target]["Dauer"] + 's';
}

function aktualisiereBoostFaktor(target) {
	eingabe[target].querySelector('.boostfaktor').innerHTML = 'x' + BoosterEigenschaften[target].Faktor;
}

