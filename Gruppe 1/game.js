'use strict';

/* BUTTONS */
var handyspiel = document.getElementById('handyspiel');
var netflix = document.getElementById('netflix');
var kino = document.getElementById('kino');
var party = document.getElementById('party');
var festival = document.getElementById('festival');

var powernap = document.getElementById('powernap');
var coffee = document.getElementById('kaffee');
var ghostwriter = document.getElementById('ghostwriter');

var pencil = document.getElementById('pencil');
var ballpen = document.getElementById('ballpen');
var pencilcase = document.getElementById('pencilcase');
var typewriter = document.getElementById('typewriter');
var phone = document.getElementById('phone');
var tablet = document.getElementById('tablet');
var macbook = document.getElementById('macbook');

var bonus = document.getElementById('bonus');

var zeichen = document.getElementById('zeichen');
var klicks = 0;
var a = 1;
var b = 1;

/* ADD EVENT LISTENERS */
handyspiel.addEventListener("mousedown", handyspielmousedown);
netflix.addEventListener("mousedown", netflixmousedown);
kino.addEventListener("mousedown", kinomousedown);
party.addEventListener("mousedown", partymousedown);
festival.addEventListener("mousedown", festivalmousedown);

powernap.addEventListener("mousedown", powernapmousedown);
ghostwriter.addEventListener("mousedown", ghostwritermousedown);
coffee.addEventListener("mousedown", coffeemousedown);

pencil.addEventListener("mousedown", pencilmousedown);
ballpen.addEventListener("mousedown", ballpenmousedown);
pencilcase.addEventListener("mousedown", pencilcasemousedown);
typewriter.addEventListener("mousedown", typewritermousedown);
phone.addEventListener("mousedown", phonemousedown);
tablet.addEventListener("mousedown", tabletmousedown);
macbook.addEventListener("mousedown", macbookmousedown);

bonus.addEventListener("mousedown", bonusmousedown);

/* FUNCTION FÜR PUNKTESTAND */
punktestand(0); // Punktestand ist am Anfang 0 

function punktestand(klicks) {
	var punkte = klicks;
	var ergebnis = 'Zeichen: ' + punkte;
    zeichen.innerHTML = '<h2>' + ergebnis + '</h2>';
    blockedbuttons();
    blockedfreizeit();
}

/* FUNCTIONS FÜR DIE FREIZEIT BUTTONS */
function handyspielmousedown() { 
    a = a + 0.1;
    animateLauneBar(a);
}

function netflixmousedown() { 
    a = a + 0.15;
    animateLauneBar(a);
}

function kinomousedown() { 
    a = a + 0.2;
    animateLauneBar(a);
}

function partymousedown() { 
    a = a + 0.25;
    animateLauneBar(a);
}

function festivalmousedown() { 
    a = a + 0.3;
    animateLauneBar(a);
}

/* FUNCTIONS FÜR DIE HELFER BUTTONS */
function coffeemousedown() { 
    b = b + 0.3;
    animateKraftBar(b)
}
function powernapmousedown() { 
    b = b + 0.3;
    animateKraftBar(b)
}

function ghostwritermousedown() { 
    let ghostTimer = setInterval(function(){
            pencilmousedown();
            setTimeout(() => {clearInterval(ghostTimer);}, 5000);
    }, 20)
}

/* FUNCTIONS FÜR DIE SCHREIB BUTTONS */
function pencilmousedown() { 
    klicks = klicks + 1;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

function ballpenmousedown() { 
    klicks = klicks + 2;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

function pencilcasemousedown() { 
    klicks = klicks + 5;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

function typewritermousedown() { 
    klicks = klicks + 10;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

function phonemousedown() { 
    klicks = klicks + 20;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

function tabletmousedown() { 
    klicks = klicks + 50;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

function macbookmousedown() { 
    klicks = klicks + 100;
    punktestand(klicks);
    a = a - 0.01;
    animateLauneBar(a);
    b = b - 0.01;
    animateKraftBar(b)
}

/* BONUS POKAL */
function bonusmousedown() { 
    klicks = klicks + 1000;
    punktestand(klicks);
    a = 1;
    animateLauneBar(a);
    b = 1;
    animateKraftBar(b)
}

/* BLOCKED BUTTONS */
function blockedbuttons() {
    if (klicks < 10) { 
        ballpen.disabled = true;  
    } else {
        ballpen.disabled = false;
    }

    if (klicks < 40) {
        pencilcase.disabled = true;
    } else {
        pencilcase.disabled = false;
    }

    if (klicks < 100) {
        typewriter.disabled = true;
    } else {
        typewriter.disabled = false;
    }

    if (klicks < 200) {
        phone.disabled = true;
    } else {
        phone.disabled = false;
    }

    if (klicks < 500) {
        tablet.disabled = true;
    } else {
        tablet.disabled = false;
    }

    if (klicks < 1000) {
        macbook.disabled = true;
    } else {
        macbook.disabled = false;
    }
}

function blockedfreizeit() {
    if (klicks < 10) { 
        handyspiel.hidden = true;  
    } else {
        handyspiel.hidden = false;
    }

    if (klicks < 20) { 
        netflix.hidden = true;  
    } else {
        netflix.hidden = false;
    }

    if (klicks < 30) { 
        kino.hidden = true;  
    } else {
        kino.hidden = false;
    }

    if (klicks < 40) { 
        party.hidden = true;  
    } else {
        party.hidden = false;
    }

    if (klicks < 50) { 
        festival.hidden = true;  
    } else {
        festival.hidden = false;
    }
}

function animateLauneBar(a) {
    document.getElementById("laune").value = a;
}

function animateKraftBar(b) {
    document.getElementById("kraft").value = b;
}