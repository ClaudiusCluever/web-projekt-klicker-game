/*
Dieses Script holt sich per AJAX online einen Pseudotext, 
der unter der Adresse https://gk-medienkompetenz.de/spieltexte/spieltexte.php?action=Schreiben abgrufen wird, 
und "schreibt" diesen Text KLick für Klick in das Div #schreibflaeche. 

Wenn #schreibflaeche voll ist (also "Seite" voll), dann zählt Seitenzahl eins hoch, 
die Seite wird geleert und das Spiel beginnt von neuem.

Dieser "Seitenumbruch" ist recht tricky: Um rauszufinden, wann die Seite voll wird, wird neben #schreibflaeche parallel auch das Div #fakeflaeche äquivalent befüllt. #fakeflaeche ist nicht zu sehen, weil es per CSS extrem außerhalb (oberhalb) des sichtbaren Bildschirms positioniert ist. #fakeflaeche hat aber dieselbe Breite wie #schreibfläche, verfügt aber im Gegensatz zu #schreibflaeche über keine definierte Höhe. Dadurch "wächst" die Höhe von #fakeflaeche langsam mit steigender Textlänge (genauso wie #schreibflaeche). Durch einen Höhenvergleich zw. #schreibflaeche und #fakeflaeche kann man jetzt aber ermitteln, wann die Seite voll ist: nämlich dann, wenn #fakeflaeche höher wird als #schreibflaeche. In diesem Fall wird die Seite erst geleert und hochgezählt und erst dann wird der nächste Textabschnitt eingefügt.
*/

$textSourceURL = "https://gk-medienkompetenz.de/spieltexte/spieltexte.php?action=Schreiben";
getWriteText();

function theGame(textObject) { 

    var schreibPunkt = 0;   // Startpunkt beim Schreiben
    var schreibLaenge = 7;  // Anzahl der Zeichen, die bei einem Klick "geschrieben" werden


    var schreibFlaeche = document.getElementById('schreibflaeche');
    var fakeFlaeche = document.getElementById('fakeflaeche');

    // 
    var seitenZahlAnzeige = document.getElementById('seitenzahl');
    var seitenzahl = 1; 
    seitenZahlAnzeige.innerHTML = seitenzahl;
    var schreibButton = pencil; 

    schreibButton.addEventListener("touchstart", function(e) {
        e.preventDefault(); 
        schreibHandlung(textObject.Text, schreibLaenge);
    });
    
    schreibButton.addEventListener("click", function(e) {
        e.preventDefault(); 
        schreibHandlung(textObject.Text, schreibLaenge);
    }); 


    function schreibHandlung(writeText, laenge) { 
        if (schreibPunkt < writeText.length) { // wenn noch nicht Textende erreicht 
            var contentBisher = schreibFlaeche.innerHTML;
            var weitererText =  writeText.slice(schreibPunkt, schreibPunkt+laenge);
            schreibPunkt = schreibPunkt + laenge;
            fakeFlaeche.innerHTML = contentBisher + weitererText;
            var fakeHoehe = fakeFlaeche.offsetHeight;
            var maxHoehe = schreibFlaeche.offsetHeight;
            if (fakeHoehe<maxHoehe) { 
                schreibFlaeche.innerHTML = contentBisher + weitererText;
            } else { 
                seitenzahl = umblaettern(seitenzahl);
            }
        } else {   // Textende erreicht!
            alert('Wow! Fertig geschrieben. Jetzt müsste neuer Text gestartet werden...');
        }
    }

    function umblaettern(seitenzahl) {
        schreibFlaeche.innerHTML = '';
        seitenzahl++;
        seitenZahlAnzeige.innerHTML = seitenzahl;
        return seitenzahl;
    }
    
}

function getWriteText() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            theGame(myObj);
        }
    };
    xmlhttp.open("GET", $textSourceURL, true);
    xmlhttp.send();
}